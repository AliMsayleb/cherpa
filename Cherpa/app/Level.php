<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function stage()
    {
        return $this->belongsTo('App\Stage');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'levels_users')->withPivot('status');
    }

    public function status()
    {
        return $this->pivot->status;
    }
}