<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function stages()
    {
        return $this->hasMany('App\Stage');
    }
}
