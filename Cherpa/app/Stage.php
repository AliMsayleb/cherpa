<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function levels()
    {
        return $this->hasMany('App\Level');
    }
}
