<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels_users', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status');
            $table->unsignedInteger('level_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('level_id')
                  ->references('id')->on('levels')
                  ->onDelte('cascade')
                  ->onUpdate('cascade');

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelte('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levels_users',function($table){
            $table->dropForeign('levels_users_level_id_foreign');
        });
        Schema::table('levels_users',function($table){
            $table->dropForeign('levels_users_user_id_foreign');
        });
        Schema::dropIfExists('levels_users');
    }
}
