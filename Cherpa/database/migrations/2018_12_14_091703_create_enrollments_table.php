<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('grade');
            $table->unsignedInteger('classroom_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelte('cascade')
                  ->onUpdate('cascade');

            $table->foreign('classroom_id')
                  ->references('id')->on('classrooms')
                  ->onDelte('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollments',function($table){
            $table->dropForeign('enrollments_user_id_foreign');
        });
        Schema::table('enrollments',function($table){
            $table->dropForeign('enrollments_classroom_id_foreign');
        });
        Schema::dropIfExists('enrollments');
    }
}
