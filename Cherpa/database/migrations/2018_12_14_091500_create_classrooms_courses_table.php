<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomsCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('classroom_id');
            $table->unsignedInteger('course_id');
            $table->timestamps();

            $table->foreign('classroom_id')
                  ->references('id')->on('classrooms')
                  ->onDelte('cascade')
                  ->onUpdate('cascade');

            $table->foreign('course_id')
                  ->references('id')->on('courses')
                  ->onDelte('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classrooms_courses',function($table){
            $table->dropForeign('classrooms_courses_classroom_id_foreign');
        });
        Schema::table('classrooms_courses',function($table){
            $table->dropForeign('classrooms_courses_course_id_foreign');
        });
        Schema::dropIfExists('classrooms_courses');
    }
}
