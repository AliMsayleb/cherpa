<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('stage_id');
            $table->timestamps();

            $table->foreign('stage_id')
                  ->references('id')->on('stages')
                  ->onDelte('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levels',function($table){
            $table->dropForeign('levels_stage_id_foreign');
        });
        Schema::dropIfExists('levels');
    }
}
